package netocaz.myapplication;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by root on 3/05/17.
 */

public interface ApiService {
    @GET("rest/Ideas/Funciones")
    Call<GithubResponse> listRepos();
}
