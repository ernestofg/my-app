package netocaz.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 3/05/17.
 */

public class Result {
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_cat")
    @Expose
    private String idCat;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCat() {
        return idCat;
    }

    public void setIdCat(String idCat) {
        this.idCat = idCat;
    }

}
