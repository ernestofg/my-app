package netocaz.myapplication;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private RecyclerView rvUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvUser = (RecyclerView) findViewById(R.id.rv_users);

        List<User> lstUsers = new ArrayList<>();
        lstUsers.add(new User(1, "ernest"));
        lstUsers.add(new User(2, "Flores"));
        lstUsers.add(new User(3, "otra persona"));

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        rvUser.setLayoutManager(llm);

        rvUser.setAdapter(new UserAdapter(lstUsers));

        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        Call<GithubResponse> call = BaseClient.provideService().listRepos();
        call.enqueue(new Callback<GithubResponse>() {
            @Override
            public void onResponse(Call<GithubResponse> call, Response<GithubResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, response.body().getResult().get(0).getNombre(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GithubResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
